=============================================
  Symja Java Swing GUI - Symbolic Math System
=============================================

Features:
* Java Swing GUI interface
* arbitrary precision integers, rational and complex numbers
* differentiation, integration, polynomials and linear algebra functions...
* function plots, parametric plots, 3D plots
* pretty printer output

Online demo: 
* http://symjaweb.appspot.com/

See the Wiki pages of the library project
* https://bitbucket.org/axelclk/symja_android_library/wiki
	
axelclk_AT_gmail_DOT_com 